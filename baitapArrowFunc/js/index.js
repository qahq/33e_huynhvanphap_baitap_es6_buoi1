const colorList =["pallet","viridian","pewter","cerulean","vermillion","lavender","celadon","saffron","fuschia","cinnabar"];

let renderListColor = (list)=>{
    let content = "";
    list.forEach((color,index) => {
        content += `<button class= "color-button ${color}" onclick="colorHouse('${color}',${index})" ></button>`;
    });
    document.getElementById("colorContainer").innerHTML = content;
};
renderListColor(colorList);
let colorHouse = (color,index)=>{
    let nutButton = document.querySelectorAll("#colorContainer button");
    nutButton.forEach(button=>{
        if(button !== nutButton[index]){
            button.classList.remove('active');
        }
    })
    nutButton[index].classList.add('active');
    document.getElementById("house").className=`house  ${color}`;
}
