let DTB = (...diemTungMonHoc) => {
  let totalScore = 0;
  diemTungMonHoc.forEach((score) => {
    totalScore += score;
  });
  return totalScore / diemTungMonHoc.length;
};

let handleTinh1 = () => {
  let diemToan = document.getElementById("inpToan").value * 1;
  let diemLy = document.getElementById("inpLy").value * 1;
  let diemHoa = document.getElementById("inpHoa").value * 1;
  document.getElementById(
    "tbKhoi1"
  ).innerHTML = `Điểm trung bình khối 1: ${DTB(
    diemToan,
    diemLy,
    diemHoa
  )}`;
};

let handleTinh2 = () => {
  let diemVan = document.getElementById("inpVan").value * 1;
  let diemSu = document.getElementById("inpSu").value * 1;
  let diemDia = document.getElementById("inpDia").value * 1;
  let diemTiengAnh = document.getElementById("inpEnglish").value * 1;
  document.getElementById("tbKhoi2").innerHTML=`Điểm trung bình khối 2: ${DTB(diemVan,diemSu,diemDia,diemTiengAnh).toLocaleString()}`
};
